import React, { Component } from 'react';

import { View, Platform, Text, ScrollView, Image, StyleSheet, ToastAndroid } from 'react-native';
import { Icon } from 'react-native-elements';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerItemList, DrawerContentScrollView } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';
import { fetchDishes, fetchComments, fetchPromos, fetchLeaders } from '../redux/ActionCreators';
import NetInfo from "@react-native-community/netinfo";
import Menu from './MenuComponent';
import Home from './HomeComponent';
import About from './AboutComponent';
import Contact from './ContactComponent';
import FavoriteComponent from './FavoriteComponent';
import Login from './LoginComponent';
import {RegisterTab} from './LoginComponent';
import Dishdetail from './DishdetailComponent';
import Reservation from './ReservationComponent';

const mapStateToProps = state => {
  return {
    dishes: state.dishes,
    comments: state.comments,
    promotions: state.promotions,
    leaders: state.leaders
  }
}

const mapDispatchToProps = dispatch => ({
  fetchDishes: () => dispatch(fetchDishes()),
  fetchComments: () => dispatch(fetchComments()),
  fetchPromos: () => dispatch(fetchPromos()),
  fetchLeaders: () => dispatch(fetchLeaders()),
})

const Stack = createStackNavigator()
const Drawer = createDrawerNavigator();

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  drawerHeader: {
    backgroundColor: '#512DA8',
    height: 140,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row'
  },
  drawerHeaderText: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold'
  },
  drawerImage: {
    margin: 10,
    width: 80,
    height: 60
  }
});

function MenuNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="Menu"
      screenOptions={{ gestureEnabled: false }}
    >    
      <Stack.Screen
        name="Menu"
        component={Menu}
        options={ props => {
            const { toggleDrawer } = props.navigation

            return ({ 
            headerStyle: {
              backgroundColor: '#512DA8',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            },
            headerLeft: (props) => {              
              return (<Icon name="menu" size={28} style = {{paddingLeft : 10}}
                color= 'white'
                onPress={ toggleDrawer } />);
            }
          });
        }
      }
      />
      <Stack.Screen
        name="Dishdetail"
        component={Dishdetail}
      />
    </Stack.Navigator>
  );
}

function ReservationNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="Reservation"
      screenOptions={{ gestureEnabled: false }}
    >
      <Stack.Screen
        name="Reservation"
        component={Reservation}
        options={ props => {
            const { toggleDrawer } = props.navigation

            return ({ 
            headerStyle: {
              backgroundColor: '#512DA8',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            },
            headerLeft: (props) => {              
              return (<Icon name="menu" size={28} style = {{paddingLeft : 10}}
                color= 'white'
                onPress={ toggleDrawer } />);
            }
          });
        }
      }
      />   
    </Stack.Navigator>
  );
}


function LoginNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{ gestureEnabled: false }}
    >
      <Stack.Screen
        name="Login"
        component={Login}
        options={ props => {
          const { toggleDrawer } = props.navigation

          return ({ 
            headerStyle: {
              backgroundColor: '#512DA8',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            },
            headerLeft: (props) => {              
              return (<Icon name="menu" size={28} style = {{paddingLeft : 10}}
                color= 'white'
                onPress={ toggleDrawer } />);
            }
          });
        }
      }
      />   
      <Stack.Screen
        name="Register"
        component={RegisterTab}
      />      
    </Stack.Navigator>
  );
}

function HomeNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{ gestureEnabled: false }}
    >
      <Stack.Screen
        name="Home"
        component={Home}
        options={ props => {
          const { toggleDrawer } = props.navigation

          return ({ 
            headerStyle: {
              backgroundColor: '#512DA8',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            },
            headerLeft: (props) => {              
              return (<Icon name="menu" size={28} style = {{paddingLeft : 10}}
                color= 'white'
                onPress={ toggleDrawer } />);
            }
          });
        }
      }
      />   
    </Stack.Navigator>
  );
}

function AboutUsNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="AboutUs"
      screenOptions={{ gestureEnabled: false }}
    >
      <Stack.Screen
        name="AboutUs"
        component={About}
        options={ props => {
            const { toggleDrawer } = props.navigation

            return ({ 
            headerStyle: {
              backgroundColor: '#512DA8',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            },
            headerLeft: (props) => {              
              return (<Icon name="menu" size={28} style = {{paddingLeft : 10}}
                color= 'white'
                onPress={ toggleDrawer } />);
            }
          });
        }
      }
      />   
    </Stack.Navigator>
  );
}

function ContactUsNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="ContactUs"
      screenOptions={{ gestureEnabled: false }}
    >
      <Stack.Screen
        name="ContactUs"
        component={Contact}
        options={ props => {
            const { toggleDrawer } = props.navigation

            return ({ 
            headerStyle: {
              backgroundColor: '#512DA8',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            },
            headerLeft: (props) => {              
              return (<Icon name="menu" size={28} style = {{paddingLeft : 10}}
                color= 'white'
                onPress={ toggleDrawer } />);
            }
          });
        }
      }
      />   
    </Stack.Navigator>
  );
}


function FavoritesNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="Favorites"
      screenOptions={{ gestureEnabled: false }}
    >
      <Stack.Screen
        name="Favorites"
        component={FavoriteComponent}
        options={ props => {
            const { toggleDrawer } = props.navigation

            return ({ 
            headerStyle: {
              backgroundColor: '#512DA8',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: "#fff"            
            },
            headerLeft: (props) => {              
              return (<Icon name="heart" size={28} style = {{paddingLeft : 10}}
                color= 'white'
                onPress={ toggleDrawer } />);
            }
          });
        }
      }
      />   
    </Stack.Navigator>
  );
}

const CustomDrawerContentComponent = (props) => (
  <ScrollView>
    <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
      <View style={styles.drawerHeader}>
        <View style={{flex:1}}>
        <Image source={require('./images/logo.png')} style={styles.drawerImage} />
        </View>
        <View style={{flex: 2}}>
          <Text style={styles.drawerHeaderText}>Ristorante Con Fusion</Text>
        </View>
      </View>
      <DrawerItemList {...props} />
    </SafeAreaView>
  </ScrollView>
);

function MainDrawer() {
  return (
    <Drawer.Navigator drawerStyle={{
        backgroundColor: '#D1C4E9'
      }}
      drawerContent={CustomDrawerContentComponent}
    >
      <Drawer.Screen name="Login" component={LoginNavigator} options={{
            drawerIcon: ({ tintColor, focused }) => <Icon
            size={24}
            color={tintColor}
            type='font-awesome'
            name='sign-in'></Icon>
      }} />       
      <Drawer.Screen name="Home" component={HomeNavigator} options={{
            drawerIcon: ({ tintColor, focused }) => <Icon
                size={24}
                color={tintColor}
                type='font-awesome'
                name='home'></Icon>
        }} />
      <Drawer.Screen name="About Us" component={AboutUsNavigator} options={{
            drawerIcon: ({ tintColor, focused }) => <Icon
                size={24}
                color={tintColor}
                type='font-awesome'
                name='info-circle'></Icon>
        }} />
      <Drawer.Screen name="Menu" component={MenuNavigator} options={{
            drawerIcon: ({ tintColor, focused }) => <Icon
                size={24}
                color={tintColor}
                type='font-awesome'
                name='list'></Icon>
        }} />
      <Drawer.Screen name="Contact Us" component={ContactUsNavigator} options={{
            drawerIcon: ({ tintColor, focused }) => <Icon
                size={24}
                color={tintColor}
                type='font-awesome'
                name='address-card'></Icon>
        }} />
      <Drawer.Screen name="Reservation" component={ReservationNavigator} options={{
            drawerIcon: ({ tintColor, focused }) => <Icon
            size={24}
            color={tintColor}
            type='font-awesome'
            name='cutlery'></Icon>
      }} />
      <Drawer.Screen name="Favorites" component={FavoritesNavigator} options={{
            drawerIcon: ({ tintColor, focused }) => <Icon
            size={24}
            color={tintColor}
            type='font-awesome'
            name='heart'></Icon>
      }} />      
       
    </Drawer.Navigator>
  );
}

class Main extends Component {

  // handleConnectivityChange = (connectionInfo) => {
  //   switch (connectionInfo.type) {
  //     case 'none':
  //       ToastAndroid.show('You are now offline!', ToastAndroid.LONG);
  //       break;
  //     case 'wifi':
  //       ToastAndroid.show('You are now connected to WiFi!', ToastAndroid.LONG);
  //       break;
  //     case 'cellular':
  //       ToastAndroid.show('You are now connected to Cellular!', ToastAndroid.LONG);
  //       break;
  //     case 'unknown':
  //       ToastAndroid.show('You now have unknown connection!', ToastAndroid.LONG);
  //       break;
  //     default:
  //       break;
  //   }
  // }  
  unsubscribe = null;

  componentDidMount() {
    this.props.fetchDishes();
    this.props.fetchComments();
    this.props.fetchPromos();
    this.props.fetchLeaders();

    unsubscribe = NetInfo.addEventListener(
      state => {
        console.log("Connection type", state.type);
        console.log("Is connected?", state.isConnected);
        switch (state.type) {
          case 'none':
            ToastAndroid.show('You are now offline!', ToastAndroid.LONG);
            break;
          case 'wifi':
            ToastAndroid.show('You are now connected to WiFi!', ToastAndroid.LONG);
            break;
          case 'cellular':
            ToastAndroid.show('You are now connected to Cellular!', ToastAndroid.LONG);
            break;
          case 'unknown':
            ToastAndroid.show('You now have unknown connection!', ToastAndroid.LONG);
            break;
          default:
            break;
        }
      }
    );

    // NetInfo.getConnectionInfo()
    //     .then((connectionInfo) => {
    //         ToastAndroid.show('Initial Network Connectivity Type: '
    //             + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType,
    //             ToastAndroid.LONG)
    //     });

    // NetInfo.addEventListener('connectionChange', this.handleConnectivityChange);
  }

  componentWillUnmount() {
    // NetInfo.removeEventListener('connectionChange', this.handleConnectivityChange);
    unsubscribe()
  }
  

  onDishSelect(dishId) {
      this.setState({selectedDish: dishId})
  }

  render() {
 
    return (
        <View style={{flex:1, paddingTop: Platform.OS === 'ios' ? 0 : 0 }}>
        <NavigationContainer>
          {/* <MenuNavigator /> */}
          <MainDrawer />
        </NavigationContainer>
        </View>        
        // <NavigationContainer>
        //   <MenuNavigator />
        // </NavigationContainer>
    );
  }
}
  
export default connect(mapStateToProps, mapDispatchToProps)(Main);