import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, Image } from 'react-native';
import { Input, CheckBox, Button, Icon } from 'react-native-elements';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { baseUrl } from '../shared/baseUrl';
import * as SecureStore from 'expo-secure-store';
import * as Permissions from "expo-permissions";
import * as ImagePicker from 'expo-image-picker';
import * as ImageManipulator from "expo-image-manipulator";
import { Asset } from "expo-asset";

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            remember: false
        }
    }

    componentDidMount() {
        SecureStore.getItemAsync('userinfo')
            .then((userdata) => {
                let userinfo = JSON.parse(userdata);
                if (userinfo) {
                    this.setState({username: userinfo.username});
                    this.setState({password: userinfo.password});
                    this.setState({remember: true})
                }
            })
    }

    static navigationOptions = {
        title: 'Login',
        tabBarIcon: ({ tintColor }) => (
            <Icon
              name='sign-in'
              type='font-awesome'            
              size={24}
              iconStyle={{ color: tintColor }}
            />
          ) 
    };

    handleLogin() {
        console.log(JSON.stringify(this.state));
        if (this.state.remember)
            SecureStore.setItemAsync('userinfo', JSON.stringify({username: this.state.username, password: this.state.password}))
                .catch((error) => console.log('Could not save user info', error));
        else
            SecureStore.deleteItemAsync('userinfo')
                .catch((error) => console.log('Could not delete user info', error));

    }

    render() {
        return (
            <View style={styles.container}>
                <Input
                    placeholder="Username"
                    leftIcon={{ type: 'font-awesome', name: 'user-o' }}
                    onChangeText={(username) => this.setState({username})}
                    value={this.state.username}
                    containerStyle={styles.formInput}
                    />
                <Input
                    placeholder="Password"
                    leftIcon={{ type: 'font-awesome', name: 'key' }}
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    containerStyle={styles.formInput}
                    />
                <CheckBox title="Remember Me"
                    center
                    checked={this.state.remember}
                    onPress={() => this.setState({remember: !this.state.remember})}
                    containerStyle={styles.formCheckbox}
                    />
                <View style={styles.formButton}>
                    <Button
                        onPress={() => this.handleLogin()}
                        title="Login"
                        icon={
                            <Icon
                                name='sign-in'
                                type='font-awesome'            
                                size={24}
                                color= 'white'
                            />
                        }
                        buttonStyle={{
                            backgroundColor: "#512DA8"
                        }}
                        />
                </View>
                <View style={styles.formButton}>
                    <Button
                        onPress={() => this.props.navigation.navigate('Register')}
                        title="Register"
                        clear
                        icon={
                            <Icon
                                name='user-plus'
                                type='font-awesome'            
                                size={24}
                                color= 'blue'
                            />
                        }
                        titleStyle={{
                            color: "blue"
                        }}
                        />
                </View>
            </View>
        );
    }

}

export class RegisterTab extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            firstname: '',
            lastname: '',
            email: '',
            remember: false,
            image: baseUrl + 'images/logo.png'
        }
    }

    getImageFromCamera = async () => {
        const {status, expires, permissions} = await Permissions.askAsync(
            Permissions.CAMERA
            ,
            Permissions.CAMERA_ROLL
        );

        if ( status === 'granted' ) {

            try {
                // let result = await ImagePicker.launchImageLibraryAsync({
                //     // mediaTypes: ImagePicker.MediaTypeOptions.All,
                //     allowsEditing: true,
                //     aspect: [4, 3],
                //     quality: 1,
                // });
                // if (!result.cancelled) {
                //     // this.setState({ image: result.uri });
                //     console.log(result);
                //     this.processImage(result.uri);
                // }        
                // console.log(result);
                const capturedImage = await ImagePicker.launchCameraAsync({
                    allowsEditing: true,
                    aspect: [4, 3],
                })
                if (!capturedImage.cancelled) {
                    // console.log(capturedImage)
                    this.processImage( capturedImage.uri )
                }                
            } catch (E) {
                console.log(E);
            }
        }
    }

    getImageFromGallery = async () => {
        const selectImage = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
        })
        if (!selectImage.cancelled) {
            console.log(selectImage)
            this.processImage(selectImage.uri)
        }
    }

    processImage = async (imageUri) => {
        let processedImage = await ImageManipulator.manipulateAsync(
            imageUri, 
            [
                {resize: {width: 400}}
            ],
            {format: 'png'}
        );
        console.log(processedImage);
        this.setState({image: processedImage.uri });

    }    
    
    static navigationOptions = {
        title: 'Register',
        tabBarIcon: ({ tintColor, focused }) => (
            <Icon
              name='user-plus'
              type='font-awesome'            
              size={24}
              iconStyle={{ color: tintColor }}
            />
          ) 
    };

    handleRegister() {
        console.log(JSON.stringify(this.state));
        if (this.state.remember){
            SecureStore.setItemAsync('userinfo', JSON.stringify({username: this.state.username, password: this.state.password}))
                .catch((error) => console.log('Could not save user info', error));
        } else {
            SecureStore.deleteItemAsync('userinfo')
            .catch( error => console.log("Couldn't delete user info due to error: ", error))
        } 
    }

    render() {
        let { image } = this.state;

        return(
            <ScrollView>
            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    <Image 
                        source={{uri: image }} 
                        loadingIndicatorSource={require('./images/logo.png')}
                        style={styles.image} 
                        />
                    <View style={styles.btnRow}>                            
                        <Button
                            title="Camera"
                            onPress={this.getImageFromCamera}
                            />
                    </View>
                    <View style={styles.btnRow}>
                        <Button
                            title = 'Gallery'
                            onPress={ this.getImageFromGallery }
                        />
                    </View>                    
                </View>
                <Input
                    placeholder="Username"
                    leftIcon={{ type: 'font-awesome', name: 'user-o' }}
                    onChangeText={(username) => this.setState({username})}
                    value={this.state.username}
                    containerStyle={styles.formInput}
                    />
                <Input
                    placeholder="Password"
                    leftIcon={{ type: 'font-awesome', name: 'key' }}
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    containerStyle={styles.formInput}
                    />
                <Input
                    placeholder="First Name"
                    leftIcon={{ type: 'font-awesome', name: 'user-o' }}
                    onChangeText={(firstname) => this.setState({firstname})}
                    value={this.state.firstname}
                    containerStyle={styles.formInput}
                    />
                <Input
                    placeholder="Last Name"
                    leftIcon={{ type: 'font-awesome', name: 'user-o' }}
                    onChangeText={(lastname) => this.setState({lastname})}
                    value={this.state.lastname}
                    containerStyle={styles.formInput}
                    />
                <Input
                    placeholder="Email"
                    leftIcon={{ type: 'font-awesome', name: 'envelope-o' }}
                    onChangeText={(email) => this.setState({email})}
                    value={this.state.email}
                    containerStyle={styles.formInput}
                    />
                <CheckBox title="Remember Me"
                    center
                    checked={this.state.remember}
                    onPress={() => this.setState({remember: !this.state.remember})}
                    containerStyle={styles.formCheckbox}
                    />
                <View style={styles.formButton}>
                    <Button
                        onPress={() => this.handleRegister()}
                        title="Register"
                        icon={
                            <Icon
                                name='user-plus'
                                type='font-awesome'            
                                size={24}
                                color= 'white'
                            />
                        }
                        buttonStyle={{
                            backgroundColor: "#512DA8"
                        }}
                        />
                </View>
            </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        margin: 20,
    },
    imageContainer: {
        flex: 1,
        flexDirection: 'row',
        margin: 20
    },
    image: {
      margin: 10,
      width: 80,
      height: 60
    },
    formInput: {
        margin: 20
    },
    formCheckbox: {
        margin: 20,
        backgroundColor: null
    },
    formButton: {
        margin: 60
    },
    btnRow: {
        alignItems: 'center',
        flexDirection: 'row',
        margin: 20
      }    
});

// const Tab = createBottomTabNavigator();

// const Login = createBottomTabNavigator({
//     Login: LoginTab,
//     Register: RegisterTab
// }, {
//     tabBarOptions: {
//         activeBackgroundColor: '#9575CD',
//         inactiveBackgroundColor: '#D1C4E9',
//         activeTintColor: '#ffffff',
//         inactiveTintColor: 'gray'
//     }
// });

// class Login extends Component {
//     render() {
//         return (
//             <Tab.Navigator>
//             <Tab.Screen name="Login" component={LoginTab} />
//             <Tab.Screen name="Register" component={RegisterTab} />
//           </Tab.Navigator>            
//         );
//     }
// }

export default Login;
// export default {RegisterTab, Login}

// import React, { Component } from 'react';
// import { View, Button, StyleSheet } from 'react-native';
// import { Card, Icon, Input, CheckBox } from 'react-native-elements';
// import * as SecureStore from 'expo-secure-store';

// class Login extends Component {

//     constructor(props) {
//         super(props);

//         this.state = {
//             username: '',
//             password: '',
//             remember: false
//         }
//     }

//     componentDidMount() {
//         SecureStore.getItemAsync('userinfo')
//             .then((userdata) => {
//                 let userinfo = JSON.parse(userdata);
//                 if (userinfo) {
//                     this.setState({username: userinfo.username});
//                     this.setState({password: userinfo.password});
//                     this.setState({remember: true})
//                 }
//             })
//     }

//     static navigationOptions = {
//         title: 'Login',
//     };

//     handleLogin() {
//         console.log(JSON.stringify(this.state));
//         if (this.state.remember)
//             SecureStore.setItemAsync('userinfo', JSON.stringify({username: this.state.username, password: this.state.password}))
//                 .catch((error) => console.log('Could not save user info', error));
//         else
//             SecureStore.deleteItemAsync('userinfo')
//                 .catch((error) => console.log('Could not delete user info', error));

//     }

//     render() {
//         return (
//             <View style={styles.container}>
//                 <Input
//                     placeholder="Username"
//                     leftIcon={{ type: 'font-awesome', name: 'user-o' }}
//                     onChangeText={(username) => this.setState({username})}
//                     value={this.state.username}
//                     containerStyle={styles.formInput}
//                     />
//                 <Input
//                     placeholder="Password"
//                     leftIcon={{ type: 'font-awesome', name: 'key' }}
//                     onChangeText={(password) => this.setState({password})}
//                     value={this.state.password}
//                     containerStyle={styles.formInput}
//                     />
//                 <CheckBox title="Remember Me"
//                     center
//                     checked={this.state.remember}
//                     onPress={() => this.setState({remember: !this.state.remember})}
//                     containerStyle={styles.formCheckbox}
//                     />
//                 <View style={styles.formButton}>
//                     <Button
//                         onPress={() => this.handleLogin()}
//                         title="Login"
//                         color="#512DA8"
//                         />
//                 </View>
//             </View>
//         );
//     }

// }

// const styles = StyleSheet.create({
//     container: {
//         justifyContent: 'center',
//         margin: 20,
//     },
//     formInput: {
//         margin: 40
//     },
//     formCheckbox: {
//         margin: 40,
//         backgroundColor: null
//     },
//     formButton: {
//         margin: 60
//     }
// });

// export default Login;